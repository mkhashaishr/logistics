import React from "react";
import { Stack } from "react-bootstrap";
import Cta2 from "../Components/Cta2";
import Cta3 from "../Components/Cta3";
import News from "../Components/News";
import Services from "../Components/Services";
import Faq from "../Components/Faq";
import Footer from "../Components/Footer";
import Cta4 from "../Components/Cta4";
import Header from "../Components/Header";

const LandingPage = () => {
    return (
        <>
            <Stack>
                <Header />
                <News />
                <Cta2 />
                <Services />
                <Cta3 />
                <Faq />
                <Cta4 />
                <Footer />
            </Stack>
        </>
    )
}

export default LandingPage;
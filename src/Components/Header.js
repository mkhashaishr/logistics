import React, {useState} from "react";
import Hero from "./Hero";
import imgLogo from "../Assets/Img/logo.svg";
import Scrollspy from "react-scrollspy";

const Header = () => {

    const [navbar, setNavbar] = useState(false);

    const changeBackround = () => {
        if (window.scrollY >= 80) {
            setNavbar(true)
        } else {
            setNavbar(false)
        }
    }

    window.addEventListener('scroll', changeBackround);

    return (
        <div>
            <div className="header" id="header">
                <nav className={navbar ?  `navbar active fixed-top navbar-expand-lg navbar-dark p-md-3 g-0` : `navbar fixed-top navbar-expand-lg navbar-dark p-md-3 g-0`}>
                    <div className="container px-2 px-lg-0">
                        <a className="navbar-brand" href="/#header">
                            <img src={imgLogo} alt="Web Logo" /> <span className="fw-bold mx-1">Logistics</span>
                        </a>
                        <button
                        className="navbar-toggler"
                        type="button"
                        data-bs-toggle="collapse"
                        data-bs-target="#navbarNav"
                        aria-controls="navbarNav"
                        aria-expanded="false"
                        aria-label="Toggle navigation"
                        >
                        <span className="navbar-toggler-icon"></span>
                        </button>

                        <div className="collapse navbar-collapse" id="navbarNav">
                        <div className="mx-auto"></div>
                        <ul className="navbar-nav py-3 py-lg-0">
                            <Scrollspy 
                                className="scrollspy d-flex flex-column flex-lg-row gap-2 gap-xl-5" items={ ['header', 'about', 'service', 'our-works', 'faq','footer'] } 
                                currentClassName="isCurrent"
                                offset={-10}
                                >
                                <li><a href="#header" className="d-flex align-items-center p-0"><span className="line"></span><span>Home</span></a></li>
                                <li><a href="#about" className="d-flex align-items-center p-0"><span className="line"></span><span>About</span></a></li>
                                <li><a href="#service" className="d-flex align-items-center p-0"><span className="line"></span><span>Service</span></a></li>
                                <li><a href="#our-works" className="d-flex align-items-center p-0"><span className="line"></span><span>Our Works</span></a></li>
                                <li><a href="#faq" className="d-flex align-items-center p-0"><span className="line"></span><span>Faq</span></a></li>
                            </Scrollspy>
                        </ul>
                        </div>
                    </div>
                </nav>
                <Hero />
            </div>
        </div>
    )
}

export default Header;
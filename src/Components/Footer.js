import React from "react";
import { Container, Row, Col, Form } from "react-bootstrap";
import { NavLink } from "react-router-dom";
import ig1 from "../Assets/Img/ig/ig1.png";
import ig2 from "../Assets/Img/ig/ig2.png";
import ig3 from "../Assets/Img/ig/ig3.png";
import ig4 from "../Assets/Img/ig/ig4.png";
import ig5 from "../Assets/Img/ig/ig5.png";
import ig6 from "../Assets/Img/ig/ig6.png";
import ig7 from "../Assets/Img/ig/ig7.png";
import ig8 from "../Assets/Img/ig/ig8.png";
import ig9 from "../Assets/Img/ig/ig9.png";
import ig10 from "../Assets/Img/ig/ig10.png";
import ig11 from "../Assets/Img/ig/ig11.png";
import ig12 from "../Assets/Img/ig/ig12.png";

const Footer = () => {
    return (
        <>
            <section id="footer" className="footer-section bg-warning">
                <Container>
                    <Row className="justify-content-between align-items-center footer-section--1 p-0 g-0">
                        <Col xl="4">
                            <h3 className="text-section-header-small text-header-footer">Newsletter</h3>
                            <p className="text-small text-footer">Sed nec libero mi. Quisque volutpat sed nibh sit amet blandit. Vivamus pulvinar vestibulum auctor. Pelle ultricies quam eros, eget accumsan nisi pulvinar vel. </p>
                        </Col>
                        <Col xl="3" className="d-flex gap-2">
                            <Form.Control type="text" className="text" placeholder="Enter your email address" />
                            <button className="btn btn-outline-primary bg-white text fw-bold text-primary">Send</button>
                        </Col>
                    </Row>
                    <Row className="p-0 g-0 footer-section--line">
                        <div className="w-100" style={{borderBottom: "1px solid #4A5568", marginBottom: "2rem"}} />
                    </Row>
                    <Row className="p-0 g-0 footer-section--2">
                        <Col md="6" xl="2">
                            <h4 className="text-small text-footer fw-bold">Information</h4>
                            <ul className="text-small">
                                <li><NavLink to="/">Terms & Conditions</NavLink></li>
                                <li><NavLink to="/">Privacy Policy</NavLink></li>
                                <li><NavLink to="/">Frequently Asked Questions</NavLink></li>
                            </ul>
                        </Col>
                        <Col md="6" xl="3" className="offset-xl-2 text-white mt-5 mt-md-0">
                            <h4 className="text-small text-footer fw-bold">Intagram</h4>
                            <p className="text-small text-footer">Follow our social media for mauris vel erat ut arcu vehicula eleifend. Pellentesque lobortis dignissim ullamcorper.</p>
                            <Row>
                                <Col lg="10" xxl="9" className="d-inline-flex flex-wrap gap-1">
                                    <img src={ig1} alt="Ig" className="img-fluid" />   
                                    <img src={ig2} alt="Ig" className="img-fluid" /> 
                                    <img src={ig3} alt="Ig" className="img-fluid" />   
                                    <img src={ig4} alt="Ig" className="img-fluid" />    
                                    <img src={ig5} alt="Ig" className="img-fluid" />
                                    <img src={ig6} alt="Ig" className="img-fluid" />   
                                    <img src={ig7} alt="Ig" className="img-fluid" /> 
                                    <img src={ig8} alt="Ig" className="img-fluid" />   
                                    <img src={ig9} alt="Ig" className="img-fluid" />    
                                    <img src={ig10} alt="Ig" className="img-fluid" />    
                                    <img src={ig11} alt="Ig" className="img-fluid" />    
                                    <img src={ig12} alt="Ig" className="img-fluid" />    
                                </Col>
                            </Row>
                        </Col>
                        <Col md="6" xl="3" className="offset-xl-1 text-white mt-5 mt-lg-0">
                            <h4 className="text-small text-footer fw-bold">Address</h4>
                            <div className="my-1">
                                <iframe src="https://www.google.com/maps/embed?pb=!1m26!1m12!1m3!1d65349045.26671011!2d-33.684656642328974!3d1.4222352227222579!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m11!3e6!4m3!3m2!1d-6.007117699999999!2d106.02875159999999!4m5!1s0x48760f864b976f3d%3A0x48aa38781ea565f8!2sstamford%20bridge!3m2!1d51.481663!2d-0.1909565!5e0!3m2!1sid!2sid!4v1638527887313!5m2!1sid!2sid" title="maps" style={{border:"0"}} allowfullscreen="" loading="lazy"></iframe>
                            </div>
                            <ul className="text-small text-footer">
                                <li>Jl Kayu Besar III Bl N-1/2-3 Kawasan Industri Miami. Kalideres, Jakarta.</li>
                                <li>Tlp & Fax. (0254) 384813/398274</li>
                                <li>contact@logistics.co.id</li>
                            </ul>
                        </Col>
                    </Row>
                </Container>
                <div className="bg-danger">
                    <Container>
                        <Row className="p-0 g-0 footer-section--3 py-3 text-white">
                            <span className="text-small  text-footer">© Copyright 2021 <b>Logistics</b></span>
                        </Row>
                    </Container>
                </div>
            </section>
        </>
    )
}

export default Footer;
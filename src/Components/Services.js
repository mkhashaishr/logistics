import React from "react";
import { NavLink } from "react-router-dom";
import { Container, Row, Col } from "react-bootstrap";
import imgService1 from "../Assets/Img/services/service1.png";
import imgService2 from "../Assets/Img/services/service2.png";
import imgService3 from "../Assets/Img/services/service3.png";
import imgService4 from "../Assets/Img/services/service4.png";

const Services = () => {
    return (
        <div>
            <section className="services-section" id="service"  style={{background: "#EDF2F7"}}>
                <Container className="p-0 g-0">
                    <Row className="justify-content-center p-3 p-lg-0">
                        <Col xl="4" className="text-center mb-2 mb-lg-5">
                            <h3 className="text-section-header-small text-header-services">Services</h3>
                            <p className="text-small section-desc">Sed suscipit augue facilisis magna blandit, vel convallis erat cursus. Quisque quis tincidunt purus.</p>
                        </Col>
                    </Row>
                    <Row className="p-3 p-lg-0 g-1 mb-5">
                        <Col xl="6">
                            <Row className="services-section--content d-flex align-items-center p-0 g-0">
                                <Col sm="6">
                                    <img src={imgService1} alt="Services 1" className="img-fluid w-100" />
                                </Col>
                                <Col sm="6" className="align-middle p-3 p-md-0">
                                    <h4>Dump Truck</h4>
                                    <p className="text-small">Ut eget ullamcorper lacus. Donec elementum est vitae tincidunt gravida. Vestibulum non nibh urna. Etiam non sapien eu urna rutrum volutpat.Mauris lacinia pellentesque massa id vestibulum.</p>
                                    <NavLink to="/">
                                        <div className="d-flex align-items-center text-small">
                                            <span>Details</span>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-arrow-right-short" viewBox="0 0 16 16">
                                                <path fill-rule="evenodd" d="M4 8a.5.5 0 0 1 .5-.5h5.793L8.146 5.354a.5.5 0 1 1 .708-.708l3 3a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708-.708L10.293 8.5H4.5A.5.5 0 0 1 4 8z"/>
                                            </svg>
                                        </div>
                                    </NavLink>
                                </Col>
                            </Row>
                        </Col>
                        <Col xl="6">
                            <Row className="services-section--content d-flex align-items-center p-0 g-0">
                                <Col sm="6">
                                    <img src={imgService2} alt="Services 1" className="img-fluid w-100" />
                                </Col>
                                <Col sm="6" className="align-middle p-3 p-md-0">
                                    <h4>Warehouse</h4>
                                    <p className="text-small">Ut eget ullamcorper lacus. Donec elementum est vitae tincidunt gravida. Vestibulum non nibh urna. Etiam non sapien eu urna rutrum volutpat.Mauris lacinia pellentesque massa id vestibulum.</p>
                                    <NavLink to="/">
                                        <div className="d-flex align-items-center text-small">
                                            <span>Details</span>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-arrow-right-short" viewBox="0 0 16 16">
                                                <path fill-rule="evenodd" d="M4 8a.5.5 0 0 1 .5-.5h5.793L8.146 5.354a.5.5 0 1 1 .708-.708l3 3a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708-.708L10.293 8.5H4.5A.5.5 0 0 1 4 8z"/>
                                            </svg>
                                        </div>
                                    </NavLink>
                                </Col>
                            </Row>
                        </Col>
                        <Col xl="6">
                            <Row className="services-section--content d-flex align-items-center p-0 g-0">
                                <Col sm="6">
                                    <img src={imgService3} alt="Services 1" className="img-fluid w-100" />
                                </Col>
                                <Col sm="6" className="align-middle p-3 p-md-0">
                                    <h4>Dump Truck</h4>
                                    <p className="text-small">Ut eget ullamcorper lacus. Donec elementum est vitae tincidunt gravida. Vestibulum non nibh urna. Etiam non sapien eu urna rutrum volutpat.Mauris lacinia pellentesque massa id vestibulum.</p>
                                    <NavLink to="/">
                                        <div className="d-flex align-items-center text-small">
                                            <span>Details</span>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-arrow-right-short" viewBox="0 0 16 16">
                                                <path fill-rule="evenodd" d="M4 8a.5.5 0 0 1 .5-.5h5.793L8.146 5.354a.5.5 0 1 1 .708-.708l3 3a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708-.708L10.293 8.5H4.5A.5.5 0 0 1 4 8z"/>
                                            </svg>
                                        </div>
                                    </NavLink>
                                </Col>
                            </Row>
                        </Col>
                        <Col xl="6">
                            <Row className="services-section--content d-flex align-items-center p-0 g-0">
                                <Col sm="6">
                                    <img src={imgService4} alt="Services 1" className="img-fluid w-100" />
                                </Col>
                                <Col sm="6" className="align-middle p-3 p-md-0">
                                    <h4>Dump Truck</h4>
                                    <p className="text-small">Ut eget ullamcorper lacus. Donec elementum est vitae tincidunt gravida. Vestibulum non nibh urna. Etiam non sapien eu urna rutrum volutpat.Mauris lacinia pellentesque massa id vestibulum.</p>
                                    <NavLink to="/">
                                        <div className="d-flex align-items-center text-small">
                                            <span>Details</span>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-arrow-right-short" viewBox="0 0 16 16">
                                                <path fill-rule="evenodd" d="M4 8a.5.5 0 0 1 .5-.5h5.793L8.146 5.354a.5.5 0 1 1 .708-.708l3 3a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708-.708L10.293 8.5H4.5A.5.5 0 0 1 4 8z"/>
                                            </svg>
                                        </div>
                                    </NavLink>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Container>
            </section>
        </div>
    )
}

export default Services;
import React from "react";
import { Container, Row, Col, Stack, Button } from "react-bootstrap";
import imgBg from "../Assets/Img/bg-ct4.png"

const Cta4 = () => {
    return (
        <>
            <div className="cta4-section">
                <div className="cta4-section--wrapper position-relative">
                    <div className="cta4-section--wrapper--img">
                        <img src={imgBg} alt="Background" />
                    </div>
                    <Container>
                        <Stack className="position-absolute top-50 start-50 translate-middle cta4-section--wrapper--center text-center">
                            <h2 className="mb-4">Lets Join with Us to Support your Logistics</h2>
                            <div>
                                <Button variant="primary" className="text text-cta4" >Fill the form</Button>
                            </div>
                        </Stack>
                    </Container>
                </div>
            </div>
        </>
    )
} 

export default Cta4;
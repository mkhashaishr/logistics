import React from "react";
import {Container, Row, Col, Stack} from "react-bootstrap"

const News = () => {
    return (
        <>
            <div className="news-section">
                <Container fluid>
                    <Row className="justify-content-end">
                        <Col xl="8" className="d-md-inline-flex gap-md-5 bg-white p-4 p-lg-5 align-items-center"  style={{borderRadius: "5px"}}>
                            <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="carousel-item active">
                                        <div class="d-md-inline-flex gap-md-5">
                                            <div className="news-section--content">
                                                <p className="text-small text-news-category">News</p>
                                                <p className="text text-news">Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. </p>
                                                <p className="text-small text-news-date">9/9/21</p>
                                            </div>
                                            <div className="news-section--content">
                                                <p className="text-small text-news-category">News</p>
                                                <p className="text text-news">Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. </p>
                                                <p className="text-small text-news-date">9/9/21</p>
                                            </div>
                                            <div className="news-section--content">
                                                <p className="text-small text-news-category">News</p>
                                                <p className="text text-news">Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. </p>
                                                <p className="text-small text-news-date">9/9/21</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="carousel-item">
                                        <div class="d-md-inline-flex gap-md-5">
                                            <div className="news-section--content">
                                                <p className="text-small text-news-category">Event</p>
                                                <p className="text text-news">Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. </p>
                                                <p className="text-small text-news-date">9/9/21</p>
                                            </div>
                                            <div className="news-section--content">
                                                <p className="text-small text-news-category">Event</p>
                                                <p className="text text-news">Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. </p>
                                                <p className="text-small text-news-date">9/9/21</p>
                                            </div>
                                            <div className="news-section--content">
                                                <p className="text-small text-news-category">Event</p>
                                                <p className="text text-news">Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. </p>
                                                <p className="text-small text-news-date">9/9/21</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="carousel-item">
                                        <div class="d-md-inline-flex gap-md-5">
                                            <div className="news-section--content">
                                                <p className="text-small text-news-category">News 3</p>
                                                <p className="text text-news">Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. </p>
                                                <p className="text-small text-news-date">9/9/21</p>
                                            </div>
                                            <div className="news-section--content">
                                                <p className="text-small text-news-category">News 3</p>
                                                <p className="text text-news">Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. </p>
                                                <p className="text-small text-news-date">9/9/21</p>
                                            </div>
                                            <div className="news-section--content">
                                                <p className="text-small text-news-category">News 3</p>
                                                <p className="text text-news">Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. </p>
                                                <p className="text-small text-news-date">9/9/21</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="news-section--arrow" style={{width: "10%"}}>
                                <Stack gap={3}>                            
                                    <svg xmlns="http://www.w3.org/2000/svg" class="bi bi-arrow-right-circle" viewBox="0 0 16 16" data-bs-target="#carouselExampleControls" data-bs-slide="next">
                                        <path fill-rule="evenodd" d="M1 8a7 7 0 1 0 14 0A7 7 0 0 0 1 8zm15 0A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM4.5 7.5a.5.5 0 0 0 0 1h5.793l-2.147 2.146a.5.5 0 0 0 .708.708l3-3a.5.5 0 0 0 0-.708l-3-3a.5.5 0 1 0-.708.708L10.293 7.5H4.5z"/>
                                    </svg>
                                    <svg xmlns="http://www.w3.org/2000/svg" class="bi bi-arrow-left-circle" viewBox="0 0 16 16" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
                                        <path fill-rule="evenodd" d="M1 8a7 7 0 1 0 14 0A7 7 0 0 0 1 8zm15 0A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-4.5-.5a.5.5 0 0 1 0 1H5.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L5.707 7.5H11.5z"/>
                                    </svg>
                                </Stack>
                            </div>
                        </Col>
                    </Row>
                </Container>
            </div>
        </>
    )
}

export default News;
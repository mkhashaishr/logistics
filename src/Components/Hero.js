import React from "react";
import imgCarousel from "../Assets/Img/bg-hero.webp";
import imgCarousel2 from "../Assets/Img/bg-hero2.jpg";
import imgCarousel3 from "../Assets/Img/bg-hero3.jpg";
import imgCarousel4 from "../Assets/Img/bg-hero4.jpg";
import imgCarousel5 from "../Assets/Img/bg-hero5.jpg";
import { Stack, Container, Button } from "react-bootstrap";

const Hero = () => {
    return (
        <>
            <div className="hero-section">                                                                 
                <div id="carouselExampleLight" className="carousel slide carousel-fade" data-bs-ride="carousel">
                    <Container className="p-0 g-0 d-none d-xl-block">
                        <div className="carousel-indicators" style={{width: "8px"}}>
                            <Stack>
                                <button type="button" data-bs-target="#carouselExampleLight" data-bs-slide-to="0" className="active rounded-circle" aria-current="true" aria-label="Slide 1"></button>
                                <button type="button" data-bs-target="#carouselExampleLight" data-bs-slide-to="1" className="rounded-circle" aria-label="Slide 2"></button>
                                <button type="button" data-bs-target="#carouselExampleLight" data-bs-slide-to="2" className="rounded-circle" aria-label="Slide 3"></button>
                                <button type="button" data-bs-target="#carouselExampleLight" data-bs-slide-to="3" className="rounded-circle" aria-label="Slide 4"></button>
                                <button type="button" data-bs-target="#carouselExampleLight" data-bs-slide-to="4" className="rounded-circle" aria-label="Slide 5"></button>
                            </Stack>
                        </div>
                    </Container>
                    <div className="carousel-inner">
                        <div className="carousel-item active" data-bs-interval="10000">
                            <img src={imgCarousel} className="d-block w-100" alt="..." />
                            <div className="overlay"></div>
                            <Container className="g-0 p-0">
                                <div className="carousel-caption h-25">
                                    <div className="caption">
                                        <h5 className="text-section-header-big">Experienced logistics professionals and customs brokers</h5>
                                        <div>
                                            <Button variant="primary" className="text-white text">More Details</Button>
                                        </div>
                                    </div>
                                </div>
                                <div className="desc d-none d-xl-block">
                                    <p className="text text-hero">We make transporting big stuff cheap and easy by helping customers directly connect with transporters who have extra truck space.</p>
                                </div>
                            </Container>
                        </div>
                        <div className="carousel-item" data-bs-interval="2000">
                            <img src={imgCarousel2} className="d-block w-100" alt="..." />
                            <div className="overlay"></div>
                            <Container className="g-0 p-0">
                                <div className="carousel-caption h-25">
                                    <div className="caption">
                                        <h5 className="text-section-header-big">In elementum felis commodo eros iaculis, at eleifend turpis congue.</h5>
                                        <button className="btn btn-primary text text-white px-4 py-2">More Details</button>
                                    </div>
                                </div>
                                <div className="desc d-none d-xl-block">
                                    <p className="text text-hero">In elementum felis commodo eros iaculis, at eleifend turpis congue. Proin feugiat finibus ipsum. Fusce molestie, eros sit amet, vitae condimentum orci nibh a purus.</p>
                                </div>
                            </Container>
                        </div>
                        <div className="carousel-item">
                            <img src={imgCarousel3} className="d-block w-100" alt="..." />
                            <div className="overlay"></div>
                            <Container className="g-0 p-0">
                                <div className="carousel-caption h-25">
                                    <div className="caption">
                                        <h5 className="text-section-header-big">Experienced logistics professionals and customs brokers</h5>
                                        <button className="btn btn-primary text text-white px-4 py-2">More Details</button>
                                    </div>
                                </div>
                                <div className="desc d-none d-xl-block">
                                    <p className="text text-hero">We make transporting big stuff cheap and easy by helping customers directly connect with transporters who have extra truck space.</p>
                                </div>
                            </Container>
                        </div>
                        <div className="carousel-item" data-bs-interval="2000">
                            <img src={imgCarousel4} className="d-block w-100" alt="..." />
                            <div className="overlay"></div>
                            <Container className="g-0 p-0">
                                <div className="carousel-caption h-25">
                                    <div className="caption">
                                        <h5 className="text-section-header-big">Nunc id velit vehicula, ullamcorper nisl ac, ultrices exit geez</h5>
                                        <button className="btn btn-primary text text-white px-4 py-2">More Details</button>
                                    </div>
                                </div>
                                <div className="desc d-none d-xl-block">
                                    <p className="text text-hero">Sed vestibulum viverra aliquet. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed tincidunt nulla urna, et porttitor velit mollis eget.</p>
                                </div>
                            </Container>
                        </div>
                        <div className="carousel-item">
                            <img src={imgCarousel5} className="d-block w-100" alt="..." />
                            <div className="overlay"></div>
                            <Container className="g-0 p-0">
                                <div className="carousel-caption h-25">
                                    <div className="caption">
                                        <h5 className="text-section-header-big">Experienced logistics professionals and customs brokers</h5>
                                        <button className="btn btn-primary text text-white px-4 py-2">More Details</button>
                                    </div>
                                </div>
                                <div className="desc d-none d-xl-block">
                                    <p className="text text-hero">We make transporting big stuff cheap and easy by helping customers directly connect with transporters who have extra truck space.</p>
                                </div>
                            </Container>
                        </div>
                    </div>
                    <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleLight" data-bs-slide="prev">
                        <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span className="visually-hidden">Previous</span>
                    </button>
                    <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleLight" data-bs-slide="next">
                        <span className="carousel-control-next-icon" aria-hidden="true"></span>
                        <span className="visually-hidden">Next</span>
                    </button>
                </div>                                
            </div>
        </>
    )
}

export default Hero;
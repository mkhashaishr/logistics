import React from "react";
import { Container, Row, Col, Stack, Button } from "react-bootstrap";
import imgCta3 from "../Assets/Img/cta3.png"

const Cta3 = () => {
    return (
        <div>
            <section className="cta3-section bg-white" id="our-works">
                <Container className="p-0 g-0">
                    <Row className="p-0 g-0 align-items-center">
                        <Col md="6" xl="4" className="text-white order-2 order-1 p-3 p-xl-0">
                            <Stack>
                                <h3 className="text-section-header-big text-header-cta3">All operations on foreign economic activity in one window</h3>
                                <p className="text text-cta3 my-4">Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.</p>
                                <div>
                                    <Button variant="primary" className="text text-white">More Details</Button>
                                </div>
                            </Stack>
                        </Col>
                        <Col md="6" xl="5" className="offset-xl-2 order-1 order-md-2 mb-3 mb-md-0 p-3 p-xl-0">
                            <Stack>
                                <img src={imgCta3} alt="Call to action 2" className="img-fluid" />
                            </Stack>
                        </Col>
                    </Row>
                </Container>
            </section>
        </div>
    )
}

export default Cta3;
import React, {useEffect} from "react";
import { Container, Row, Col, Stack } from "react-bootstrap";

const Faq = () => {

    useEffect(() => {
        document.title = `Logistics`;
    });

    return (
        <div>
            <section className="faq-section" id="faq" style={{background: "#EDF2F7"}}>
                <Container className="p-0 g-0">
                    <Row className="justify-content-center p-0 g-0">
                        <Col xl="4" className="text-center">
                            <h3 className="text-section-header-small text-header-services">Frequently Asked Questions</h3>
                            <p className="text-small section-desc">Sed eros odio, pulvinar in egestas eget, congue ut enim. In hac habitasse platea dictumst.</p>
                        </Col>
                    </Row>
                    <Row className="p-0 g-0 faq-section--wrapper justify-content-center">
                        <Col xl="6">
                            <Stack gap={2}>
                                <div className="p-2 p-lg-0">
                                    <p>
                                        <a className="btn w-100" style={{background: "white", borderRadius: "10px"}} data-bs-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                                            <div className="d-flex align-items-center" style={{height: "3.75rem"}}>
                                                <span className="text text-faq">Etiam faucibus elit a quam vestibulum lacinia. Phasellus at odio justo ?</span>
                                                <span className="ms-auto">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-caret-down-fill" viewBox="0 0 16 16">
                                                        <path d="M7.247 11.14 2.451 5.658C1.885 5.013 2.345 4 3.204 4h9.592a1 1 0 0 1 .753 1.659l-4.796 5.48a1 1 0 0 1-1.506 0z"/>
                                                    </svg>
                                                </span>
                                            </div>
                                        </a>
                                    </p>
                                    <div className="collapse" id="collapseExample">
                                        <div className="card card-body" style={{borderRadius: "10px"}}>
                                            <span className="text text-faq">
                                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Perspiciatis minima vitae illum, deleniti facilis ipsam neque odit tenetur. Sit, voluptas. Some placeholder content for the collapse component. This panel is hidden by default but revealed when the user activates the relevant trigger.
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="p-2 p-lg-0">
                                    <p>
                                        <a className="btn w-100" style={{background: "white", borderRadius: "10px"}} data-bs-toggle="collapse" href="#collapseExample2" role="button" aria-expanded="false" aria-controls="collapseExample">
                                            <div className="d-flex align-items-center" style={{height: "3.75rem"}}>
                                                <span className="text text-faq">Nullam ultrices orci quis quam elementum suscipit vel et odio ?</span>
                                                <span className="ms-auto">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-caret-down-fill" viewBox="0 0 16 16">
                                                        <path d="M7.247 11.14 2.451 5.658C1.885 5.013 2.345 4 3.204 4h9.592a1 1 0 0 1 .753 1.659l-4.796 5.48a1 1 0 0 1-1.506 0z"/>
                                                    </svg>
                                                </span>
                                            </div>
                                        </a>
                                    </p>
                                    <div className="collapse" id="collapseExample2">
                                        <div className="card card-body" style={{borderRadius: "10px"}}>
                                            <span className="text text-faq">
                                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Perspiciatis minima vitae illum, deleniti facilis ipsam neque odit tenetur. Sit, voluptas. Some placeholder content for the collapse component. This panel is hidden by default but revealed when the user activates the relevant trigger.
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="p-2 p-lg-0">
                                    <p>
                                        <a className="btn w-100" style={{background: "white", borderRadius: "10px"}} data-bs-toggle="collapse" href="#collapseExample3" role="button" aria-expanded="false" aria-controls="collapseExample">
                                            <div className="d-flex align-items-center" style={{height: "3.75rem"}}>
                                                <span className="text text-faq">Etiam faucibus elit a quam vestibulum lacinia. Phasellus at odio justo ?</span>
                                                <span className="ms-auto">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-caret-down-fill" viewBox="0 0 16 16">
                                                        <path d="M7.247 11.14 2.451 5.658C1.885 5.013 2.345 4 3.204 4h9.592a1 1 0 0 1 .753 1.659l-4.796 5.48a1 1 0 0 1-1.506 0z"/>
                                                    </svg>
                                                </span>
                                            </div>
                                        </a>
                                    </p>
                                    <div className="collapse" id="collapseExample3">
                                        <div className="card card-body" style={{borderRadius: "10px"}}>
                                            <span className="text text-faq">
                                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Perspiciatis minima vitae illum, deleniti facilis ipsam neque odit tenetur. Sit, voluptas. Some placeholder content for the collapse component. This panel is hidden by default but revealed when the user activates the relevant trigger.
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="p-2 p-lg-0">
                                    <p>
                                        <a className="btn w-100" style={{background: "white", borderRadius: "10px"}} data-bs-toggle="collapse" href="#collapseExample4" role="button" aria-expanded="false" aria-controls="collapseExample">
                                            <div className="d-flex align-items-center" style={{height: "3.75rem"}}>
                                                <span className="text text-faq">Nullam ultrices orci quis quam elementum suscipit vel et odio ?</span>
                                                <span className="ms-auto">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-caret-down-fill" viewBox="0 0 16 16">
                                                        <path d="M7.247 11.14 2.451 5.658C1.885 5.013 2.345 4 3.204 4h9.592a1 1 0 0 1 .753 1.659l-4.796 5.48a1 1 0 0 1-1.506 0z"/>
                                                    </svg>
                                                </span>
                                            </div>
                                        </a>
                                    </p>
                                    <div className="collapse" id="collapseExample4">
                                        <div className="card card-body" style={{borderRadius: "10px"}}>
                                            <span className="text text-faq">
                                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Perspiciatis minima vitae illum, deleniti facilis ipsam neque odit tenetur. Sit, voluptas. Some placeholder content for the collapse component. This panel is hidden by default but revealed when the user activates the relevant trigger.
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="p-2 p-lg-0">
                                    <p>
                                        <a className="btn w-100" style={{background: "white", borderRadius: "10px"}} data-bs-toggle="collapse" href="#collapseExample5" role="button" aria-expanded="false" aria-controls="collapseExample">
                                            <div className="d-flex align-items-center" style={{height: "3.75rem"}}>
                                                <span className="text text-faq">Etiam faucibus elit a quam vestibulum lacinia. Phasellus at odio justo ?</span>
                                                <span className="ms-auto">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-caret-down-fill" viewBox="0 0 16 16">
                                                        <path d="M7.247 11.14 2.451 5.658C1.885 5.013 2.345 4 3.204 4h9.592a1 1 0 0 1 .753 1.659l-4.796 5.48a1 1 0 0 1-1.506 0z"/>
                                                    </svg>
                                                </span>
                                            </div>
                                        </a>
                                    </p>
                                    <div className="collapse" id="collapseExample5">
                                        <div className="card card-body" style={{borderRadius: "10px"}}>
                                            <span className="text text-faq">
                                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Perspiciatis minima vitae illum, deleniti facilis ipsam neque odit tenetur. Sit, voluptas. Some placeholder content for the collapse component. This panel is hidden by default but revealed when the user activates the relevant trigger.
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </Stack>
                        </Col>
                    </Row>
                </Container>
            </section>
        </div>
    )
}

export default Faq;
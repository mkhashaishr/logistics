import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom";
import LandingPage from "../Pages/LandingPage";

const Routes = () => {
    return (
        <>
            <Router>
                <Switch>
                    <Route exact component={LandingPage} path="/" />
                </Switch>
            </Router>
        </>
    )
}

export default Routes;